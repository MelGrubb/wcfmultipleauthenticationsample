﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WcfMultipleAuthenticationSample.Contracts.DataContracts;
using WcfMultipleAuthenticationSample.Contracts.ServiceContracts;
using WcfMultipleAuthenticationSample.Tests.Framework;

namespace WcfMultipleAuthenticationSample.Tests
{
    [TestClass]
    public class PingServiceTests
    {
        [TestInitialize]
        public void SetUp()
        {
            PingService = new ServiceFactory<IPingService>().GetServiceProxy();
        }

        public IPingService PingService { get; set; }

        [TestMethod]
        public void Ping_perceives_caller_anonymously()
        {
            var response = PingService.Ping(new PingRequest { Message = "Testing" });
            Assert.AreEqual("Testing", response.Message);
            Assert.IsFalse(response.IsAuthenticated);
        }
    }

    [TestClass]
    public class PingServiceSecureTests
    {
        [TestInitialize]
        public void SetUp()
        {
            PingService = new ServiceFactory<IPingService>().GetServiceProxy();
        }

        public IPingService PingService { get; set; }

        [TestMethod]
        public void Ping_sees_caller_identity()
        {
            var response = PingService.Ping(new PingRequest { Message = "Testing" });
            Assert.AreEqual("Testing", response.Message);
            Assert.IsTrue(response.IsAuthenticated);
        }

        [TestMethod]
        public void PingSecure_sees_caller_identity()
        {
            var response = PingService.PingSecure(new PingRequest { Message = "Testing" });
            Assert.AreEqual("Testing", response.Message);
            Assert.IsTrue(response.IsAuthenticated);            
        }
    }
}
