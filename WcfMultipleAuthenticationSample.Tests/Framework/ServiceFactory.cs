﻿using System;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;

namespace WcfMultipleAuthenticationSample.Tests.Framework
{
    public interface IServiceFactory<out TServiceInterface>
    {
        /// <summary>Returns a proxy for the service interface specified by <typeparam name="TServiceInterface">TServiceInterface</typeparam>.</summary>
        /// <returns>An instance of a proxy class which implements the specified interface.</returns>
        /// <remarks>This is very convention-based. If you vary from the IServiceName/ServiceName pattern, this won't work.</remarks>
        TServiceInterface GetServiceProxy();
    }

    public abstract class ServiceFactory
    {
        protected static readonly Binding Binding;
        protected static readonly string UrlPattern;

        static ServiceFactory()
        {
            var bindingTypeName = ConfigurationManager.AppSettings["ServiceBindingType"];
            var bindingType = AppDomain.CurrentDomain.GetAssemblies()
                .Single(a => a.GetName().Name == "System.ServiceModel")
                .GetType(bindingTypeName, false, true);
            var bindingName = ConfigurationManager.AppSettings["ServiceBindingName"];
            
            Binding = (Binding)Activator.CreateInstance(bindingType, bindingName);
            UrlPattern = ConfigurationManager.AppSettings["ServiceUrlPattern"];
        }
    }

    public class ServiceFactory<TServiceInterface> : ServiceFactory, IServiceFactory<TServiceInterface>
    {
        private static readonly ChannelFactory<TServiceInterface> ChannelFactory;

        static ServiceFactory()
        {
            var url = Regex.Replace(string.Format(UrlPattern, typeof(TServiceInterface).Name.Substring(1)), "Secure.svc$", ".svc/secure");
            ChannelFactory = new ChannelFactory<TServiceInterface>(Binding, new EndpointAddress(url));
        }

        public TServiceInterface GetServiceProxy()
        {
            var channel = ChannelFactory.CreateChannel();
            return channel;
        }
    }
}