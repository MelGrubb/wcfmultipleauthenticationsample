﻿using System.Security.Claims;
using System.Security.Permissions;
using System.ServiceModel.Activation;
using WcfMultipleAuthenticationSample.Contracts.DataContracts;
using WcfMultipleAuthenticationSample.Contracts.ServiceContracts;

namespace WcfMultipleAuthenticationSample.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PingService : IPingService
    {
        private const string GroupName = @"Users";

        public PingResponse Ping(PingRequest request)
        {
            var principal = ClaimsPrincipal.Current;
            var identity = principal.Identity;
            var isDomainUser = principal.IsInRole(GroupName);

            return new PingResponse(request)
            {
                Message = request.Message,
                Principal = principal.ToString(),
                Identity = identity.ToString(),
                IdentityName = principal.Identity.Name,
                IsAuthenticated = isDomainUser
            };
        }

        [PrincipalPermission(SecurityAction.Demand, Role = GroupName)]
        public PingResponse PingSecure(PingRequest request)
        {
            return Ping(request);
        }
    }
}