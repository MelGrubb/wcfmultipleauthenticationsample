﻿using System.Security.Claims;
using System.ServiceModel.Activation;
using WcfMultipleAuthenticationSample.Contracts.DataContracts;
using WcfMultipleAuthenticationSample.Contracts.ServiceContracts;

namespace WcfMultipleAuthenticationSample.Services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PongService : IPongService
    {
        public PongResponse Pong(PongRequest request)
        {
            var principal = ClaimsPrincipal.Current;
            var identity = principal.Identity;
            var isAuthenticated = identity.IsAuthenticated;

            return new PongResponse(request)
            {
                Message = request.Message,
                Principal = principal.ToString(),
                Identity = identity.ToString(),
                IdentityName = principal.Identity.Name,
                IsAuthenticated = isAuthenticated
            };
        }
    }
}