﻿As of v4.5, WCF has "Multiple Authentication Support", which means that it SHOULD be possible to call the same service
using more than one authentication scheme (e.g. Windows, Basic, Anonymous). This sample is meant to illustrate the key
to making this all work, if only I could figure out what that is.

According to the articles I've read, all you're supposed to have to do is enable more than one authentication scheme in
IIS, and then specify "InheritedFromHost" for the clientCredentialType in the web.config.

The Problem
===========
What I have found, however, is that if anonymous authentication is enabled, then no attempt will be made to authenticate
the caller at all, even if the service method is decorated with the PrincipalPermission attribute. To illustrate this,
the same service method is implemented twice, once with no PrincipalPermission attribute (Ping), and once with a 
PrincipalPermission attribute which demands that the user be a member of the local machine's "Users" group (PingSecure).
Both methods take a PingRequest and return a PingResponse which illustrates how the caller is perceived by the method.

The authentication modes of the IISExpress host can be set by selecting the project in the solution explorer, and toggling
the "Anonymous Authentication" and "Windows Authentication" settings. The service can then be tested in the "WCF Test 
Client" by selecting the PingService.svc file in the Solution Explorer, and pressing F5.

If only Windows authentication is enabled, then calls to either method (i.e. Ping, PingSecure) will return a response 
showing that the user is perceived as a WindowsPrincipal, including their name and that they are a member of the local 
"Users" group. 

If both Anonymous and Windows authentication are enabled, then calls to the Ping method will show that the user is 
anonymous, as expected, but calls to PingSecure will fail with an AccessDenied error.

The Goal
========
My goal is to set it up a service API so that anonymous extranet users can call the Ping method, but only internal domain
users can call the PingSecure method.

The Solution
============
It's not what I was originally aiming for, but it seems to work well enough. Extracting the "secure" methods into a second
interface which inherits the first results in a WSDL showing both interfaces. The methods in the unsecured "Public" 
interfaces are available to anyone, and are called anonymously. Methods in the derived interface can be exposed with 
different bindings so that they are only available to authenticated users.