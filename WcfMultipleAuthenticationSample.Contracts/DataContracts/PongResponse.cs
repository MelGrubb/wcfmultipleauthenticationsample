﻿using System;
using System.Runtime.Serialization;

namespace WcfMultipleAuthenticationSample.Contracts.DataContracts
{
    [DataContract]
    public class PongResponse
    {
        [DataMember]
        public string Identity { get; set; }

        [DataMember]
        public string IdentityName { get; set; }

        [DataMember]
        public bool IsAuthenticated { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Principal { get; set; }

        [DataMember]
        public Guid RequestId { get; set; }

        public PongResponse()
        {
        }

        public PongResponse(PongRequest request)
        {
            RequestId = request.RequestId;
            Message = request.Message;
        }
    }
}