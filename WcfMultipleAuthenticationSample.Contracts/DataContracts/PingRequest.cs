﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace WcfMultipleAuthenticationSample.Contracts.DataContracts
{
    [DataContract]
    public class PingRequest
    {
        [DataMember]
        [Required, StringLength(20)]
        public string Message { get; set; }

        [DataMember]
        public Guid RequestId { get; set; }

        public PingRequest()
        {
            RequestId = Guid.NewGuid();
        }
    }
}