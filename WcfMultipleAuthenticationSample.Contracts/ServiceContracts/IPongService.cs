﻿using System.ServiceModel;
using WcfMultipleAuthenticationSample.Contracts.DataContracts;

namespace WcfMultipleAuthenticationSample.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IPongServicePublic
    {
        [OperationContract]
        PongResponse Pong(PongRequest request);
    }

    [ServiceContract]
    public interface IPongService : IPongServicePublic
    {
        // PongService has no "secured" methods, so it adds nothing
    }
}