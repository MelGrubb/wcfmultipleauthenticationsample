﻿using System.ServiceModel;
using WcfMultipleAuthenticationSample.Contracts.DataContracts;

namespace WcfMultipleAuthenticationSample.Contracts.ServiceContracts
{
    [ServiceContract]
    public interface IPingServicePublic
    {
        [OperationContract]
        PingResponse Ping(PingRequest request);
    }

    [ServiceContract]
    public interface IPingService : IPingServicePublic
    {
        // PingService adds one additional method, which only works for authenticated users.

        [OperationContract]
        PingResponse PingSecure(PingRequest request);
    }
}